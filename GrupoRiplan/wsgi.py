"""
WSGI config for GrupoRiplan project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.contrib.auth.models import User
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "GrupoRiplan.settings")

application = get_wsgi_application()


if User.objects.filter(username='admin').count() == 0:
    User.objects.create_superuser(
        username="admin",
        email="admin@admin.com",
        password='admin123'
    )