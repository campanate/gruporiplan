# coding=utf-8
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView, RedirectView

from conteudo.forms import ContatoForm
from conteudo.models import Banner, Conteudo, Projeto, Depoimento, Resultado




class Home(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        c = super(Home, self).get_context_data(**kwargs)
        layout = {
            'header': 'bg-orange',
            'title': 'Home',
            'class_logo': 'home',
            'projetos': Projeto.objects.all(),

        }
        c['layout'] = layout
        c['banners'] = Banner.objects.filter(ativo=True).order_by('-pk')
        c['empresa'] = Conteudo.objects.filter(tipo=1).first()
        c['depoimentos'] = Depoimento.objects.all().order_by('-pk')[:4]
        c['numeros'] = Resultado.objects.all().order_by('-pk')[:3]
        return c


class Planejamento(TemplateView):
    template_name = 'planejamento.html'

    def get_context_data(self, **kwargs):
        c = super(Planejamento, self).get_context_data(**kwargs)
        layout = {
            'header': 'bg-gray',
            'title': 'Planejamento de Projetos',
            'projetos': Projeto.objects.filter(pagina=3),
            'class_logo': 'gp',
            'logo': 'gp-logo.png',
            'color': 'white'
        }
        c['layout'] = layout
        c['conteudo'] = Conteudo.objects.filter(tipo=3).first()
        return c


class InstalacaoEletrica(TemplateView):
    template_name = 'planejamento.html'

    def get_context_data(self, **kwargs):
        c = super(InstalacaoEletrica, self).get_context_data(**kwargs)
        layout = {
            'header': 'bg-yellow',
            'title': 'Instalação Elétrica',
            'class_logo': 'ie',
            'logo': 'ie-logo.png',
            'projetos': Projeto.objects.filter(pagina=2),
            'color': 'black'
        }
        c['layout'] = layout
        c['conteudo'] = Conteudo.objects.filter(tipo=2).first()
        return c


class EnergiaSolar(TemplateView):
    template_name = 'planejamento.html'

    def get_context_data(self, **kwargs):
        c = super(EnergiaSolar, self).get_context_data(**kwargs)
        layout = {
            'header': 'bg-orange',
            'title': 'Energia Solar',
            'logo': 'es-logo.png',
            'class_logo': 'es',
            'projetos': Projeto.objects.filter(pagina=5),
            'color': 'white'
        }
        c['layout'] = layout
        c['conteudo'] = Conteudo.objects.filter(tipo=5).first()
        return c


class Capacitacao(TemplateView):
    template_name = 'planejamento.html'

    def get_context_data(self, **kwargs):
        c = super(Capacitacao, self).get_context_data(**kwargs)
        layout = {
            'header': 'bg-blue',
            'title': 'Capacitação',
            'logo': 'cp-logo.png',
            'class_logo': 'cp',
            'projetos': Projeto.objects.filter(pagina=4),
            'color': 'white'
        }
        c['layout'] = layout
        c['conteudo'] = Conteudo.objects.filter(tipo=4).first()
        return c


@csrf_exempt
def newContact(request):
    if request.method == "POST":
        form = ContatoForm(data=request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({'status': 'success'})
    return JsonResponse({'status': 'error'})
