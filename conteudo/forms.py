from django import forms

from conteudo.models import Conteudo, Contato, Resultado


class ResultadoForm(forms.ModelForm):

    class Meta:
        model = Resultado
        fields = ['numero', 'texto']
        widgets = {
            'numero': forms.Textarea(),
            'texto': forms.Textarea()
        }

    class Media:
        js = ('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
              '//cdn.ckeditor.com/4.6.2/basic/ckeditor.js',
              'js/forms.js',)

class ConteudoForm(forms.ModelForm):

    class Meta:
        model = Conteudo
        fields = ['tipo', 'texto', 'primeira_bola', 'segunda_bola']
        widgets = {
            'primeira_bola': forms.Textarea(),
            'segunda_bola': forms.Textarea(),
            'texto': forms.Textarea()
        }


    def clean(self):
        print(self)
        return super(ConteudoForm, self).clean()

    class Media:
        js = ('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
              '//cdn.ckeditor.com/4.6.2/basic/ckeditor.js',
              'js/forms.js',)



class ContatoForm(forms.ModelForm):

    class Meta:
        model = Contato
        fields = ['nome', 'email', 'assunto', 'mensagem']