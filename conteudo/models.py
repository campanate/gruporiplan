# coding=utf-8
from __future__ import unicode_literals

from django.db import models

CONTEUDO_TIPO = (
    (1, "Empresa"),
    (2, "Instalação Elétrica"),
    (3, "Planejamento de Projetos"),
    (4, "Capacitação"),
    (5, "Energia Solar")
)


class Banner(models.Model):
    titulo = models.CharField(
        max_length=100,
        verbose_name="Título"
    )

    imagem = models.ImageField(
        upload_to='banners/',
        verbose_name="Imagem"
    )

    ativo = models.BooleanField(
        default=True,
        verbose_name="Ativo"
    )

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return unicode(self.titulo)

    class Meta:
        db_table = 'banners'
        verbose_name = "Banner"
        verbose_name_plural = 'Banners'


class Depoimento(models.Model):
    nome = models.CharField(
        verbose_name="Nome",
        max_length=100
    )

    empresa = models.CharField(
        max_length=100,
        verbose_name="Empresa"
    )

    foto = models.ImageField(
        verbose_name="Foto",
        upload_to='depoimentos/'
    )

    texto = models.CharField(
        max_length=1000,
        verbose_name="Texto"
    )

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return unicode(self.nome)

    class Meta:
        db_table = 'depoimentos'
        verbose_name = 'Depoimento'
        verbose_name_plural = 'Depoimentos'


class Projeto(models.Model):
    titulo = models.CharField(
        max_length=100,
        verbose_name="Título"
    )

    imagem = models.ImageField(
        upload_to="projetos/"
    )

    pagina = models.IntegerField(
        choices=CONTEUDO_TIPO,
        verbose_name="Referente à",
        default=2
    )

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return unicode(self.titulo)

    class Meta:
        db_table = 'projetos'
        verbose_name = 'Projeto'
        verbose_name_plural = 'Projetos'


class Resultado(models.Model):
    numero = models.CharField(
        max_length=1000,
        verbose_name="Número"
    )

    texto = models.CharField(
        max_length=1000,
        verbose_name="Texto"
    )

    def __str__(self):
        return self.numero

    def __unicode__(self):
        return unicode(self.numero)

    class Meta:
        db_table = 'numeros'
        verbose_name = 'Resultado'
        verbose_name_plural = 'Resultados'


class Contato(models.Model):
    def __str__(self):
        return self.nome

    def __unicode__(self):
        return unicode(self.nome)

    nome = models.CharField(
        max_length=100,
        verbose_name="Nome"
    )

    email = models.EmailField(
        max_length=100,
        verbose_name="E-mail"
    )

    assunto = models.CharField(
        max_length=100,
        verbose_name="Assunto"
    )

    mensagem = models.CharField(
        max_length=1000,
        verbose_name="Mensagem"
    )

    class Meta:
        db_table = 'contatos'
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'


class Conteudo(models.Model):
    tipo = models.IntegerField(
        verbose_name="Tipo",
        choices=CONTEUDO_TIPO
    )

    texto = models.CharField(
        verbose_name="Texto Principal",
        max_length=3000
    )

    primeira_bola = models.CharField(
        verbose_name="Texto da primeira bola",
        null=True,
        blank=True,
        max_length=3000
    )

    segunda_bola = models.CharField(
        verbose_name="Texto da segunda bola",
        null=True,
        blank=True,
        max_length=3000
    )

    def __str__(self):
        return u"Conteúdo"

    def __unicode__(self):
        return u"Conteúdo"

    class Meta:
        db_table = 'conteudos'
        verbose_name = 'Conteudo',
        verbose_name_plural = 'Conteúdos'
