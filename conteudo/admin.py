from django.contrib import admin

from conteudo.forms import ConteudoForm, ResultadoForm
from conteudo.models import Banner, Depoimento, Projeto, Resultado, Contato, Conteudo


class BannerAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'ativo']


class DepoimentoAdmin(admin.ModelAdmin):
    list_display = ['nome', 'empresa']


class ProjetoAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'pagina']


class ResultadoAdmin(admin.ModelAdmin):
    list_display = ['numero']
    form = ResultadoForm


class ContatoAdmin(admin.ModelAdmin):
    list_display = ['nome', 'email', 'assunto']


class ConteudoAdmin(admin.ModelAdmin):
    list_display = ['tipo']
    form = ConteudoForm


admin.site.register(Conteudo, ConteudoAdmin)
admin.site.register(Contato, ContatoAdmin)
admin.site.register(Resultado, ResultadoAdmin)
admin.site.register(Projeto, ProjetoAdmin)
admin.site.register(Depoimento, DepoimentoAdmin)
admin.site.register(Banner, BannerAdmin)
